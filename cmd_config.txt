[Execution]
scope = set_finance_database

[set_finance_database]
db_filename = FinanceDatabase
webInfoFile = WebAccessInf_text

[set_finance_database_variables]
db_filename = FinanceDatabase

[exploratory_data_analysis_stage]
filename = TempletonEmergingMarkets_All
database_folder = .\FinanceDatabase\TempletonEmergingMarkets

[features_selection_engineering]
train_file = TempletonEmergingMarkets_All_train
test_file = TempletonEmergingMarkets_All_test
model_path = None
database_folder = .\FinanceDatabase\TempletonEmergingMarkets

[learning_analysis]
filename = train
database_folder = database

[ts_training_stage]
raw_data_file = TempletonEmergingMarkets_All_train
database_folder = .\FinanceDatabase\TempletonEmergingMarkets
train_file_tail_tag = _cat_group
features_engineering_model_path = None
model_config_filename = models_config_test

[fbprophet_training_stage]
raw_data_file = TempletonEmergingMarkets_All_train
database_folder = .\FinanceDatabase\TempletonEmergingMarkets
train_file_tail_tag = _cat_group
features_engineering_model_path = None
model_config_filename = models_config_test

[model_organization]
model_path = .\ML_output\
pred_id_column=None
pred_column=y
data_drop_column=None
pred_filename=None
databasefolder=database