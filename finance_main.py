# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 11:49:50 2018

@author: user009
DataVer  - Author - Note
20181003 - C.Y. Peng - First Release
20190212 - C.Y. Peng - Second Release, MongoDB Data Strore
20190221 - C.Y. Peng - Setting: Output Folder and ML Prediction
20190309 - C.Y. Peng - addFinance Data Scraping and Analysis Tool Version - Release
20200107 - C.Y. Peng - cmd mode
20200114 - C.Y. Peng - ML model training
20200209 - C.Y. Peng - Add the time sereis analysis
20200216 - C.Y. Peng - Third Release
"""
from sys import path
#path.append('../../CommonLib/')
#path.append('ExchangeRateManager/')
#path.append('FundManager/')
import CommonUtility as CU
from configparser import ConfigParser

import H5FileManager as H5FM
import MongoDBManager
import FinanceMongoDB
import finance_preprocessing as FP
import finance_model_creator as finance_model
import regMultiModelTool as MMT
import sys
import os
import re

def FineanceMLToolVer():
    author = "C.Y. Peng"
    ver = "20200119"
    build = "01r"
    print("Finance Machine Learning Prediction Tool Information:\n")
    print("Author:" + author + "\n")
    print("Version:" + ver + "-" + build + "\n")

def set_finance_database(db_filename = "database", 
                         webinfofile = "WebAccessInf"):
    FP.SetFundDataset(db_filename, webInfoFile = webinfofile)

def set_finance_database_variables(db_filename = "database"):
    MongoDBInfoObj = FP.MongoDBLogin(db_filename)
    FP.SetFundEDA(db_filename, MongoDBInfoObj)

def exploratory_data_analysis_stage(filename = "database",
                                    database_folder = "database"):
    FP.EDABasicProfile(filename, database_folder)
    FP.EDABasicStatistics(filename, database_folder)
    try:
        FP.EDABasicStatisticsLabeledCorr(filename, database_folder)
    except:
        pass

def learning_analysis(filename = "train", 
                      database_folder = "database"):
    FP.EDAComponentsAnalysis(filename, database_folder)
    FP.EDADTDumpROC(filename, database_folder)

def features_selection_engineering(train_file = "train",
                                   test_file = "test",
                                   model_path = os.path.join(".","EDA_output","train"),
                                   database_folder = "database"):
    print(train_file, database_folder, test_file, database_folder, model_path)
    FP.FeaturesEngineering(train_file, database_folder)
    FP.FeaturesEngineeringFit(test_file, database_folder, model_path)

def ts_training_stage(raw_data_file = 'train',
                      database_folder = "database",
                      train_file_tail_tag = None,
                      features_engineering_model_path = os.path.join(".","EDA_output","train"),
                      model_config_filename='models_config_test'):
    finance_model.ts_cv_training_stage(raw_data_file,
                                       database_folder,
                                       train_file_tail_tag = train_file_tail_tag,
                                       features_engineering_model_path = features_engineering_model_path,
                                       model_config_filename =model_config_filename)    

def fbprophet_training_stage(raw_data_file = 'train',
                             database_folder = "database",
                             train_file_tail_tag = None,
                             features_engineering_model_path = os.path.join(".","EDA_output","train"),
                             model_config_filename='models_config_test'):
    #finance_model.test()
    finance_model.fbprophet_cv_training_stage(raw_data_file,
                                              database_folder,
                                              train_file_tail_tag = train_file_tail_tag,
                                              features_engineering_model_path = features_engineering_model_path,
                                              model_config_filename =model_config_filename)

def training_stage(raw_data_file = 'train',
                   database_folder = "database",
                   train_file_tail_tag = None,
                   features_engineering_model_path = os.path.join(".","EDA_output","train"),
                   model_config_filename='models_config_test'):
    
    finance_model.nested_training_stage(raw_data_file,
                                        database_folder,
                                        train_file_tail_tag = train_file_tail_tag,
                                        features_engineering_model_path = features_engineering_model_path,
                                        model_config_filename =model_config_filename)

def model_organization(model_path = ".\ML_output", **kwargs):
    ds_column = ['Date']
    ts_model_column = ['NAV']
    MMT.model_organization(model_path, ds_column, ts_model_column, **kwargs)

def main():
    FineanceMLToolVer()
    mode = check_env()
    if (1 == mode):
        cmd_mode()
    if (2 == mode):
        ide_mode()
    #webcam_application()

def voting_prediction_stage_para_setting(config):
    kewgs = {}
    for para in config[config['Execution']['scope']]:
        if ('database_folder'== para) or ('train_filename'== para) or ('test_filename'== para) or ('refit' == para):
            kewgs[para] = config[config['Execution']['scope']][para]
        elif "column_selector_list" == para:
            kewgs[para] = []
            string_array = config[config['Execution']['scope']][para].split(';')
            for idx, string_list in enumerate(string_array):
                kewgs[para].append(ast.literal_eval(string_list))
        else:
            kewgs[para] = ast.literal_eval(config[config['Execution']['scope']][para])
    print('Execution Scope {}'.format(config['Execution']['scope']))
    print('Parameters Setting {}'.format(kewgs))
    return kewgs

def cmd_mode():
    # from ConfigParser import ConfigParser # for python3
    data_file = sys.argv[1]
    config = ConfigParser()
    config.read(data_file)
    kewgs = {}
    print(config.sections())
    for para in config[config['Execution']['scope']]:
        pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
        flag = pattern.match(config[config['Execution']['scope']][para])
        if flag:
            try:
                kewgs[para] = float(config[config['Execution']['scope']][para])
            except:
                pass
            try:
                kewgs[para] = int(config[config['Execution']['scope']][para])
            except:
                pass
        else:
            kewgs[para] = config[config['Execution']['scope']][para]
    """
    for item in config.sections():
        kewgs[item] = {}
        for para in config[item]:
            kewgs[item][para] = config[item][para]
    """
    print(kewgs)
    if 'set_finance_database' == config['Execution']['scope']:
        set_finance_database(**kewgs)

    if 'set_finance_database_variables' == config['Execution']['scope']:
        set_finance_database_variables(**kewgs)

    if 'exploratory_data_analysis_stage' == config['Execution']['scope']:
        exploratory_data_analysis_stage(**kewgs)

    if 'features_selection_engineering' == config['Execution']['scope']:
        features_selection_engineering(**kewgs)

    if 'learning_analysis' == config['Execution']['scope']:
        learning_analysis(**kewgs)

    if 'fbprophet_training_stage' == config['Execution']['scope']:
        fbprophet_training_stage(**kewgs)

    if 'ts_training_stage' == config['Execution']['scope']:
        ts_training_stage(**kewgs)

    if 'training_stage' == config['Execution']['scope']:
        training_stage(**kewgs)

    if 'model_organization' == config['Execution']['scope']:
        model_organization(**kewgs)

    print('Completed!')

def ide_mode():
    set_finance_database()
    set_finance_database_variables()
    exploratory_data_analysis_stage()
    features_selection_engineering()
    ts_training_stage()
    fbprophet_training_stage()
    training_stage()
    model_organization()

def check_env():
    print('Check environment variables: {}'.format(sys.argv))
    if (len(sys.argv) > 1):
        print("CMD Mode, Author: CY")
        return 1
    else:
        print("IDE Mode, Author: CY")
        return 2

if __name__== "__main__":
    main()
