'''
@Author: CY Peng
@Date: 2020-01-03
@LastEditTime: 2020-01-03

DataVer  - Author - Note
2020-01-03 - CY - Initialization       

'''

# Common Lib
import os
import glob
import pandas as pd
import numpy as np
import h5py
import pickle
import matplotlib.pyplot as plt
from pandas.plotting import table
import seaborn as sns
import re
from matplotlib.colors import ListedColormap

# Custom Lib
import CommonUtility as CU
import H5FileManager as H5FM
import MongoDBManager

# Finance Lib
import FinanceMongoDB
import ExchangeRate as GER
import getFundMainFunc as gFMF
import getFundStatistics as gFS
import getFundFBMLPrediction as gFFBMLP

# Custom ML Lib
import ExploratoryDataAnalysis as EDA
import LearningAnalysis as LA
import PreprocessingUtility as PU
import SamplingLib as SM
import ModelEvaluation as ME
import tsModelCreatorHelper as tsMCH

#here = os.path.dirname(os.path.realpath(__file__))
DBOutputFolder = 'FinanceDatabase'
CU.createOutputFolder(DBOutputFolder)
outputFolder = 'EDA_output'
CU.createOutputFolder(outputFolder)
here = os.path.dirname(os.path.realpath(__file__))

"""
Basic Preprocessing Utility
"""
def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def MongoDBLogin(filename):
    client, database_name = MongoDBManager.mongoDB_login()
    enable_database_flag = 0
    if (client != None) and (database_name != None):
        enable_database_flag = 1
        FinanceMongoDB.check_mongoDB_database(filename, client, database_name)
    else:
        print("Do Not Provide the Any Database Address for These Data!")
    MongoDBInfoObj = (client, database_name, enable_database_flag)
    return MongoDBInfoObj

def SetFundDataset(filename, webInfoFile = 'WebAccessInf'):
    SetExchangeRate(filename)
    SetFundData(filename, DBOutputFolder, here, webInfoFile = webInfoFile)

def SetFundEDA(filename, MongoDBInfoObj):
    SetFundStatisticData(filename, MongoDBInfoObj)

def EDABasicProfile(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    EDA.df_data_check(data, outputFolder, filename)
    EDA.df_data_type_check(data, outputFolder, filename)

"""
Basic Data Analysis Utility
"""
def df_data_numerical_data_distribution_plot_check(df_data, check_tag, raw_num_data_type, num2cat_data_type, filename, fig_size):
    EDA.df_data_multi_type_plot(df_data.dropna(), raw_num_data_type, None, outputFolder, filename+check_tag, 
                                plot_type = 'Distribution', ylabel_list = ["No. of Passenger(s)"], fig_size = fig_size)
    #EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), num2cat_data_type, None, outputFolder, filename+check_tag, plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = fig_size)

def df_data_categorical_data_distribution_plot_check(df_data, check_tag, raw_cat_data_type, filename, fig_size):
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), raw_cat_data_type, None, outputFolder, filename+check_tag, 
                                plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = fig_size)

def EDAComponentsAnalysis(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    data = data.drop(columns=['Date', 'y'],axis = 1)
    pcaObj = LA.PrincipleComponentAnalysis(data, outputFolder, filename)
    LA.VisualizingN2MEigenVector(pcaObj, data, (0, 5), outputFolder, filename)

def EDADTDumpROC(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    y = data.loc[:, 'y'].to_frame()
    X = data.drop(columns=['Date', 'y'], axis=1)
    ME.df_data_features_ranking_roc_curve(X, y, outputFolder, filename)

"""
Basic Data Scraping from Web
"""
def SetExchangeRate(filename):
    # Exchange Rate
    GER.main(filename, DBOutputFolder)
    #H5FM.H5ReSave(setFileName)

def SetFundData(filename, DBOutputFolder, webInfoPath, webInfoFile = 'WebAccessInf'):
    # Fund Data
    gFMF.main(filename, DBOutputFolder, webInfoPath, webInfoFile = webInfoFile)

"""
Basic Exploratory Data Analysis
"""
def SetFundStatisticData(filename, MongoDBInfoObj):
    # Initialition
    client, database_name, enable_database_flag = MongoDBInfoObj
    
    # Fund Statistic Data
    gFS.main(filename, 'FundList', 'Fund', DBOutputFolder)
    
    if (enable_database_flag):
        FinanceMongoDB.upload_data_to_mongoDB(filename, client, database_name)

def num_basic_processing(df_data):
    # Prediction
    #df_data['y'] = df_data.loc[:, 'NAV'].shift(periods=-1)
    return df_data

def cat_basic_processing(df_data):
    # Long/Short
    df_data['Long/Short_0Map'] = df_data.loc[:, 'Long/Short_0'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_1Map'] = df_data.loc[:, 'Long/Short_1'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_2Map'] = df_data.loc[:, 'Long/Short_2'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_3Map'] = df_data.loc[:, 'Long/Short_3'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_4Map'] = df_data.loc[:, 'Long/Short_4'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_5Map'] = df_data.loc[:, 'Long/Short_5'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_6Map'] = df_data.loc[:, 'Long/Short_6'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_7Map'] = df_data.loc[:, 'Long/Short_7'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_8Map'] = df_data.loc[:, 'Long/Short_8'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    df_data['Long/Short_9Map'] = df_data.loc[:, 'Long/Short_9'].map({'super-S+': 0, 'super-S': 1, 'S+': 2, 'S': 3, 'L->S': 4, 'N': 5, 'S->L': 6, 'L': 7, 'L+': 8, 'super-L': 9, 'super-L+': 10, '0':5}).astype(int)
    
    # Bull/Bear
    df_data['Bull/Bear_0Map'] = df_data.loc[:, 'Bull/Bear_0'].map({'Bear': 0, 'Turn-Bear': 1, 'N': 2, 'Turn-Bull': 3, 'Bull': 4, '0':2}).astype(int)
    df_data['Bull/Bear_1Map'] = df_data.loc[:, 'Bull/Bear_1'].map({'Bear': 0, 'Turn-Bear': 1, 'N': 2, 'Turn-Bull': 3, 'Bull': 4, '0':2}).astype(int)
    df_data['Bull/Bear_2Map'] = df_data.loc[:, 'Bull/Bear_2'].map({'Bear': 0, 'Turn-Bear': 1, 'N': 2, 'Turn-Bull': 3, 'Bull': 4, '0':2}).astype(int)
    #df_data['Bull/Bear_3Map'] = df_data.loc[:, 'Bull/Bear_3'].map({'Bear': 0, 'Turn-Bear': 1, 'N': 2, 'Turn-Bull': 3, 'Bull': 4, '0':2}).astype(int)
	
    # Change Point
    LS_cat_list = ['Long/Short_0Map','Long/Short_1Map','Long/Short_2Map','Long/Short_3Map','Long/Short_4Map','Long/Short_5Map','Long/Short_6Map','Long/Short_7Map','Long/Short_8Map','Long/Short_9Map']
    df_data['Change_Point'] = 0
    for idx in range(0, df_data.shape[0]):
        #print(df_data.loc[idx, LS_cat_list])
        #print((df_data.loc[idx, LS_cat_list] >= 7).sum())
        if ((((df_data.loc[idx, LS_cat_list] >= 7)).sum() >= 7 ) or (((df_data.loc[idx, LS_cat_list] <= 3).sum() >= 7))):
            df_data.loc[idx, 'Change_Point'] = 1    
        
    return df_data
    
def eda_basic_statistics_data_type_def():
    # numerical data type
    raw_num_data_type = ['NAV', 'Bias-5', 'Bias-10','Bias-20', 'Bias-70', 'MTM', 'MA, Week', 'MA, Half of Month', 'MA, Month', 'MA, Season', 'MA, Year', 'OBV', 'DI', 'EMA12', 'EMA26', 'DIF', 'MACD', 'OSC', 'PSY-12', 'min', 'Q1', 'Q2', 'Q3', 'Max', 'Up-5', 'Down-5', 'RS-5', 'RSI-5', 'Up-10', 'Down-10', 'RS-10', 'RSI-10', 'AF', 'SAR-10', 'K_0', 'WR-5', 'WR-10', 'WR-15', 'RSV-9', 'K_1', 'D']
    num2cat_data_type = []
    num_data_type_ylabel = []
    	
    # categorical data type
    raw_cat_data_type = ['Long/Short_0', 'Long/Short_1', 'Long/Short_2',  'Long/Short_3', 'Long/Short_4', 
	                     'Long/Short_5', 'Long/Short_6', 'Long/Short_7', 'Long/Short_8', 'Long/Short_9',
	                     'Bull/Bear_0', 'Bull/Bear_1', 'Bull/Bear_2']
    map_cat_data_type = ['Long/Short_0Map', 'Long/Short_1Map', 'Long/Short_2Map',  'Long/Short_3Map', 'Long/Short_4Map', 
	                     'Long/Short_5Map', 'Long/Short_6Map', 'Long/Short_7Map', 'Long/Short_8Map', 'Long/Short_9Map',
	                     'Bull/Bear_0Map', 'Bull/Bear_1Map', 'Bull/Bear_2Map', 'Change_Point']
    cat_data_type_yLabel_list = ['']
	
	# label data type
    label_data_type = ['y']
    drop_out_data_type = [''] # Categorical Data
    return raw_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type, drop_out_data_type

def EDABasicStatistics(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)
    raw_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type, drop_out_data_type = eda_basic_statistics_data_type_def()
    # NAV plot
    EDABasicPlot(df_data, filename)
    EDAFinanceIndicatorsPlot(df_data, filename)
    EDATimeSeriesAnalysisBasicPlot(df_data, filename)
    EDATimeSeriesAnalysisDecomposition(df_data, filename)

    # Data Check
    df_data_numerical_data_distribution_plot_check(df_data.dropna(), '_basic', raw_num_data_type, num2cat_data_type, filename, (10, 10))
    df_data_categorical_data_distribution_plot_check(df_data, '_basic', raw_cat_data_type, filename, (10, 10))

    # Numerical Data Type
    #EDA.df_data_ANOVA_test(df_data.dropna(), raw_num_data_type, outputFolder, filename)
    EDA.df_data_numerical_type_describe(df_data, outputFolder, filename) #.fillna(value=-1)
    #EDA.df_data_multi_type_plot(df_data.fillna(value=-1), numerical_data_type, None, outputFolder, filename, 
    #                            plot_type = 'Distribution', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))
    EDA.df_data_numerical_type_correlation_heatmap(df_data.dropna(), raw_num_data_type, outputFolder, filename)
    
    #EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), bi_categorical_data_type, None, outputFolder, filename+'_single', plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))
    
    # All Data Type
    #EDA.df_data_percentage_description(df_data.fillna(value="unknown"), None, outputFolder, filename)
	
def EDABasicPlot(df_data, filename):
    df_data['DateStamp'] = pd.to_datetime(df_data['Date'], errors='coerce').dt.to_pydatetime()
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = filename.split("_")[0] + ' Net Asset Value', layout = 'tightly', rot=90)
    fig = lines.get_figure()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('Net Asset Value, NAV')
    fig.savefig(os.path.join(outputFolder, filename + "_line.svg"))

def EDAFinanceIndicatorsPlot(df_data, filename):
    specs = EDA.multiplot_strategy(11)
    specslist = []
    for idx, spec in enumerate(specs):
        specslist.append(spec)

    ax = plt.subplot(specslist[0])
    df_data['DateStamp'] = pd.to_datetime(df_data['Date'], errors='coerce').dt.to_pydatetime()
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Quartile', legend = 'Net Asset Value, NAV', rot=90, ax=ax, figsize=(35, 20))
    lines = df_data[::-1].loc[df_data['Q1']>0,['DateStamp', 'Q1']].plot.line(x='DateStamp', y='Q1', legend = 'Q1', rot=90, ax=lines) #, layout = 'tightly'
    lines = df_data[::-1].loc[df_data['Q2']>0,['DateStamp', 'Q2']].plot.line(x='DateStamp', y='Q2', legend = 'Q2', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['Q3']>0,['DateStamp', 'Q3']].plot.line(x='DateStamp', y='Q3', legend = 'Q3', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with Quartile')

    ax = plt.subplot(specslist[1])
    df_data['DateStamp'] = pd.to_datetime(df_data['Date'], errors='coerce').dt.to_pydatetime()
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Moving Average', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['MA, Week']>0,['DateStamp', 'MA, Week']].plot.line(x='DateStamp', y='MA, Week', legend = 'Weekly Moving Average', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['MA, Half of Month']>0,['DateStamp', 'MA, Half of Month']].plot.line(x='DateStamp', y='MA, Half of Month', legend = 'Semi-Monthly Moving Average', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['MA, Month']>0,['DateStamp', 'MA, Month']].plot.line(x='DateStamp', y='MA, Month', legend = 'Monthly Moving Average', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['MA, Season']>0,['DateStamp', 'MA, Season']].plot.line(x='DateStamp', y='MA, Season', legend = 'Seasonly Moving Average', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['MA, Year']>0,['DateStamp', 'MA, Year']].plot.line(x='DateStamp', y='MA, Year', legend = 'Yearly Moving Average', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with MA') 

    #df_data['DateStamp'] = pd.to_datetime(df_data['Date'], errors='coerce').dt.to_pydatetime()
    ax = plt.subplot(specslist[2])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Parabolic Stop and Reverse', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['SAR-10']>0,['DateStamp', 'SAR-10']].plot.line(x='DateStamp', y='SAR-10', legend = '10-Day Parabolic Stop and Reverse', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with SAR')	

    ax = plt.subplot(specslist[3])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Psychological Line', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['PSY-12']>0,['DateStamp', 'PSY-12']].plot.line(x='DateStamp', y='PSY-12', legend = '12-Day Psychological Line', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with PSY')	

    ax = plt.subplot(specslist[4])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'On Balance Volume', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['OBV']>0,['DateStamp', 'OBV']].plot.line(x='DateStamp', y='OBV', legend = 'On Balance Volume Line', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with OBV')		

    ax = plt.subplot(specslist[5])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'KD Line', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['K_1']>0,['DateStamp', 'K_1']].plot.line(x='DateStamp', y='K_1', legend = 'K Line', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['D']>0,['DateStamp', 'D']].plot.line(x='DateStamp', y='D', legend = 'D Line', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with KD')	

    ax = plt.subplot(specslist[6])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Williams Percent Range Line', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['WR-5']>0,['DateStamp', 'WR-5']].plot.line(x='DateStamp', y='WR-5', legend = '5-Day Williams Percent Range Line', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['WR-10']>0,['DateStamp', 'WR-10']].plot.line(x='DateStamp', y='WR-10', legend = '10-Day Williams Percent Range Line', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['WR-15']>0,['DateStamp', 'WR-15']].plot.line(x='DateStamp', y='WR-15', legend = '15-Day Williams Percent Range Line', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with WR')

    ax = plt.subplot(specslist[7])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Momentum', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['MTM']>0,['DateStamp', 'MTM']].plot.line(x='DateStamp', y='MTM', legend = 'Momentum', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with Momentum')

    ax = plt.subplot(specslist[8])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Relative Strength Index', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['RSI-5']>0,['DateStamp', 'RSI-5']].plot.line(x='DateStamp', y='RSI-5', legend = '5-Day Relative Strength Index', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['RSI-10']>0,['DateStamp', 'RSI-10']].plot.line(x='DateStamp', y='RSI-10', legend = '10-Day Relative Strength Index', rot=90, ax=lines)    
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with RSI')

    ax = plt.subplot(specslist[9])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Oscillator Indicator', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['OSC']>0,['DateStamp', 'OSC']].plot.line(x='DateStamp', y='OSC', legend = 'Oscillator Indicator', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with OSC')
	
    ax = plt.subplot(specslist[10])
    lines = df_data[::-1].plot.line(x='DateStamp', y='NAV', title = 'Bias', legend = 'Net Asset Value, NAV', rot=90, ax=ax)
    lines = df_data[::-1].loc[df_data['Bias-5']>0,['DateStamp', 'Bias-5']].plot.line(x='DateStamp', y='Bias-5', legend = '5-Day Bias Line', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['Bias-10']>0,['DateStamp', 'Bias-10']].plot.line(x='DateStamp', y='Bias-10', legend = '10-Day Bias Line', rot=90, ax=lines)
    lines = df_data[::-1].loc[df_data['Bias-20']>0,['DateStamp', 'Bias-20']].plot.line(x='DateStamp', y='Bias-20', legend = '20-Day Bias Line', rot=90, ax=lines)
    fig = lines.get_figure()
    plt.legend()
    plt.grid()
    plt.xlabel('Date, Year')
    plt.ylabel('NAV with Bias')
    
    EDA.multiplot_save(fig, outputFolder, filename + "_finance_indicator_line.svg")

def EDATimeSeriesAnalysisBasicPlot(df_data, filename):
    fig = plt.figure(figsize=(25, 10))
    ax = plt.subplot(211)
    fig = tsMCH.ARIMA_ACF_plot(df_data, 'NAV', lags = 25, title = "ACF Plot", ax = ax)
    plt.grid()
    ax = plt.subplot(212)
    fig = tsMCH.ARIMA_PACF_plot(df_data, 'NAV', lags = 25, title = "PACF Plot", ax = ax)
    plt.grid()
    fig.savefig(os.path.join(outputFolder, filename + "_tsBasic.svg"))
    plt.close()

    fig = plt.figure(figsize=(25, 10))
    ax = plt.subplot(211)
    fig = tsMCH.ARIMA_ACF_plot(df_data.loc[:, 'NAV'].diff(periods=1).to_frame(name="NAV_Diff").dropna(), 'NAV_Diff', lags = 25, title = "ACF Plot", ax = ax)
    plt.grid()
    ax = plt.subplot(212)
    fig = tsMCH.ARIMA_PACF_plot(df_data.loc[:, 'NAV'].diff(periods=1).to_frame(name="NAV_Diff").dropna(), 'NAV_Diff', lags = 25, title = "PACF Plot", ax = ax)
    plt.grid()
    fig.savefig(os.path.join(outputFolder, filename + "_tsDiffBasic.svg"))
    plt.close()
	
    ACFtest = tsMCH.ADF_test(df_data, 'NAV')
    f = open(os.path.join(outputFolder, filename + "_acfTest.txt"), "a")
    f.write('ADF Score: {}\n'.format(ACFtest[0]))
    f.write('pvalue: {}\n'.format(ACFtest[1]))
    f.write('usedlag: {}\n'.format(ACFtest[2]))
    f.write('nobs: {}\n'.format(ACFtest[3]))
    f.write('critical values: {}\n'.format(ACFtest[4]))
    f.write('icbest: {}\n'.format(ACFtest[5]))
    #f.write('resstore: {}'.format(ACFtest[6]))
    f.close()

    #rint(df_data.loc[:, 'NAV'].diff(periods=1).dropna())
    #print(df_data.loc[:, 'NAV'].diff(periods=1).to_frame(name="NAV_Diff").dropna())
	
    ACFtest = tsMCH.ADF_test(df_data.loc[:, 'NAV'].diff(periods=1).to_frame(name="NAV_Diff").dropna(), 'NAV_Diff')
    f = open(os.path.join(outputFolder, filename + "_diff_acfTest.txt"), "a")
    f.write('ADF Score: {}\n'.format(ACFtest[0]))
    f.write('pvalue: {}\n'.format(ACFtest[1]))
    f.write('usedlag: {}\n'.format(ACFtest[2]))
    f.write('nobs: {}\n'.format(ACFtest[3]))
    f.write('critical values: {}\n'.format(ACFtest[4]))
    f.write('icbest: {}\n'.format(ACFtest[5]))
    #f.write('resstore: {}'.format(ACFtest[6]))
    f.close()

def EDATimeSeriesAnalysisDecomposition(df_data, filename): 
    decomposed_volume = tsMCH.decomposition(df_data, 'NAV', period=360, model='additive')
    df_decomposed_volume = pd.DataFrame({'Date': df_data['Date'],
                                         'Observed': decomposed_volume.observed,
										 'Seasonal': decomposed_volume.seasonal,
									     'Trend': decomposed_volume.trend,
                                         'Resid': decomposed_volume.resid})
    fig = plt.figure(figsize=(25, 10))
    ax = plt.subplot(211)
    fig = tsMCH.ARIMA_ACF_plot(df_decomposed_volume, 'Trend', lags = 25, title = "ACF Plot", ax = ax)
    plt.grid()
    ax = plt.subplot(212)
    fig = tsMCH.ARIMA_PACF_plot(df_decomposed_volume, 'Trend', lags = 25, title = "PACF Plot", ax = ax)
    plt.grid()
    fig.savefig(os.path.join(outputFolder, filename + "_tsTrendBasic.svg"))
    plt.close()

    fig = plt.figure(figsize=(25, 10))
    ax = plt.subplot(211)
    fig = tsMCH.ARIMA_ACF_plot(df_decomposed_volume.loc[:, 'Trend'].diff(periods=1).to_frame(name="Trend_Diff").dropna(), 'Trend_Diff', lags = 25, title = "ACF Plot", ax = ax)
    plt.grid()
    ax = plt.subplot(212)
    fig = tsMCH.ARIMA_PACF_plot(df_decomposed_volume.loc[:, 'Trend'].diff(periods=1).to_frame(name="Trend_Diff").dropna(), 'Trend_Diff', lags = 25, title = "PACF Plot", ax = ax)
    plt.grid()
    fig.savefig(os.path.join(outputFolder, filename + "_tsTrendDiffBasic.svg"))
    plt.close()

    fig = plt.figure(figsize=(10, 10))
    ax = plt.subplot(511)
    lines = df_decomposed_volume.plot.line(y='Observed', title = 'Time Series Decomposition', rot=90, ax=ax)
    fig = lines.get_figure()
    plt.gca().get_legend().remove()
    plt.setp(ax.get_xticklabels(), visible=False)
    plt.grid()
    #plt.xlabel('Date')
    plt.ylabel('Observed')

    ax = plt.subplot(512)
    #lines = df_decomposed_volume[::-1].plot.line(x='Date', y='Observed', title = 'Time Series Decomposition', legend = 'Observed', rot=90, ax=ax)
    lines = df_decomposed_volume.Observed.diff().plot.line(y='Observed', rot=90, ax=ax)
    fig = lines.get_figure()
    #plt.gca().get_legend().remove()
    plt.setp(ax.get_xticklabels(), visible=False)
    plt.grid()
    #plt.xlabel('Date')
    plt.ylabel('Observed Diff')
	
    ax = plt.subplot(513)
    lines = df_decomposed_volume.plot.line(y='Trend', rot=90, ax=ax)
    #lines = df_decomposed_volume_diff.plot.line(x='Date', y='Trend', legend = 'Trend Diff', rot=90, ax=ax)
    fig = lines.get_figure()
    plt.gca().get_legend().remove()
    plt.setp(ax.get_xticklabels(), visible=False)
    #plt.legend()
    plt.grid()
    #plt.xlabel('Date')
    plt.ylabel('Trend')

    ax = plt.subplot(514)
    lines = df_decomposed_volume.plot.line(y='Seasonal', rot=90, ax=ax)
    fig = lines.get_figure()
    plt.gca().get_legend().remove()
    plt.setp(ax.get_xticklabels(), visible=False)
    plt.grid()
    #plt.xlabel('Date')
    plt.ylabel('Seasonal')

    ax = plt.subplot(515)
    lines = df_decomposed_volume.plot.line(x='Date', y='Resid', rot=90, ax=ax)
    fig = lines.get_figure()
    plt.gca().get_legend().remove()
    plt.grid()
    plt.xlabel('Date')
    plt.ylabel('Residual')
    
    fig.savefig(os.path.join(outputFolder, filename + "_tsDecomposition.svg"))
    plt.close()

    fig = plt.figure(figsize=(35, 25))
    ax = plt.subplot(111)
    lines = df_decomposed_volume.plot.line(x='Date', y='Trend', legend = 'Trend', rot=90, ax=ax)
    df_decomposed_volume.Trend.diff(periods = 1).plot(legend = 'Trend Diff')
    fig = lines.get_figure()
    #plt.legend()
    plt.grid()
    plt.xlabel('Date')
    plt.ylabel('Trend')

    fig.savefig(os.path.join(outputFolder, filename + "_tsTrendAnalysis.svg"))
    plt.close()

def EDABasicStatisticsLabeledCorr(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)
    raw_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type, drop_out_data_type = eda_basic_statistics_data_type_def()
	
	# Numerical Data Type
    EDA.df_data_multi_type_plot(df_data.dropna(), raw_num_data_type, label_data_type[0], outputFolder, filename, 
                                plot_type = 'Box', ylabel_list = numerical_data_type_ylabel, fig_size = (10, 10))
    EDA.df_data_numerical_type_pairscatter_plot(df_data.fillna(value=-1), raw_num_data_type, label_data_type[0],
                                                outputFolder, filename)

    # Categorical Data Type
    EDA.df_data_percentage_description(df_data.fillna(value="unknown"), label_data_type[0], outputFolder, filename)
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), raw_cat_data_type, label_data_type[0], outputFolder, filename,plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))     
    EDA.df_data_multi_type_percentage_value_bar_plot(df_data.fillna(value="unknown"), raw_cat_data_type,
                                                     label_data_type[0], outputFolder, filename, ylabel_list = None)

"""
Features Engineering based on the Basic EDA
"""
def features_transformation(df_data, filename, model_file_path):
    num_data_type = ['Bias-5', 'Bias-10','Bias-20', 'Bias-70', 'MTM', 'MA, Week', 'MA, Half of Month', 
                     'MA, Month', 'MA, Season', 'MA, Year', 'OBV', 'DI', 'EMA12', 'EMA26', 'DIF', 
                     'MACD', 'OSC', 'PSY-12', 'min', 'Q1', 'Q2', 'Q3', 'Max', 'Up-5', 'Down-5', 'RS-5', 
                     'RSI-5', 'Up-10', 'Down-10', 'RS-10', 'RSI-10', 'AF', 'SAR-10', 'K_0', 'WR-5', 
                     'WR-10', 'WR-15', 'RSV-9', 'K_1', 'D', 'NAV']
    cat_data_type = ['Long/Short_0Map', 'Long/Short_1Map', 'Long/Short_2Map',  'Long/Short_3Map', 'Long/Short_4Map', 
                     'Long/Short_5Map', 'Long/Short_6Map', 'Long/Short_7Map', 'Long/Short_8Map', 'Long/Short_9Map',
                     'Bull/Bear_0Map', 'Bull/Bear_1Map', 'Bull/Bear_2Map']
    label_data_type = ['y']
    
    num_data = df_data.loc[:, num_data_type]
    cat_data = df_data.loc[:, cat_data_type]
    return num_data, cat_data

def FeaturesEngineering(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)
    label_data_type = ['y']

    # Feature Transformation
    num_data, cat_data = features_transformation(df_data, filename, None)

    # Original Data
    df_data.to_csv(os.path.join(database_folder, filename + "_features_engineering.csv"), index=False)

    # Data Concat
    num_group = pd.concat([df_data.loc[:,'Date'], num_data, df_data.loc[:,label_data_type]],axis = 1)
    cat_group = pd.concat([df_data.loc[:,'Date'], cat_data, df_data.loc[:,label_data_type]],axis = 1)
    all_group = pd.concat([df_data.loc[:,'Date'], num_data, cat_data, df_data.loc[:,label_data_type]],axis = 1)

    # Data Storing
    num_group.to_csv(os.path.join(database_folder, filename + "_num_group.csv"), index=False)
    cat_group.to_csv(os.path.join(database_folder, filename + "_cat_group.csv"), index=False)
    all_group.to_csv(os.path.join(database_folder, filename + "_all_group.csv"), index=False)

def FeaturesEngineeringFit(filename, database_folder, model_file_path):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)

    # Feature Transformation
    num_data, cat_data = features_transformation(df_data, filename, None)

    # Original Data
    df_data.to_csv(os.path.join(database_folder, filename + "_features_engineering.csv"), index=False)

    # Data Concat
    num_group = pd.concat([df_data.loc[:,'Date'], num_data],axis = 1)
    cat_group = pd.concat([df_data.loc[:,'Date'], df_data.loc[:,'NAV'], cat_data],axis = 1)
    all_group = pd.concat([df_data.loc[:,'Date'], num_data, cat_data],axis = 1)

    # Data Storing
    num_group.to_csv(os.path.join(database_folder, filename + "_num_group.csv"), index=False)
    cat_group.to_csv(os.path.join(database_folder, filename + "_cat_group.csv"), index=False)
    all_group.to_csv(os.path.join(database_folder, filename + "_all_group.csv"), index=False)