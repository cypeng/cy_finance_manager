# Finance Manager: Fund Data Web Scraping, Analysis and ML Prediction Algorithm
<!-- blank line -->
<dl>
　<dd>C.Y. Peng</dd>
<!-- blank line -->
　<dd>Keywords: Data Science, Web Data Scraping, Exploratory Data Analysis, Feature Engineering, Machine Learning, Time Series Analysis</dd> 
</dl> 

## Lib Information
- Release Ver: 20200216-R 1.0.0
- Lib Ver: 20200216
- Author: C.Y. Peng
- Required Lib: requirements_venv36.txt
- OS Required: Windows 64 bit

## Part I. Overview
It is difficult to track fund data every day and analyze data from the historical information, it is easy to reach the fund data scraping from the web and analyze data through the Finance Fund Data Web Scraping and Analysis Tool.
In this project, we cover several functions as follows:
- [x] Project Overview
- [x] Fundemental Principle
   - [x] Introduction to the Data Science
   - [x] Fund Data Analysis Overview
- [x] Data Analysis through the Data Science  
   - [x] From EDA to Model Established
   - [x] Simple Model Example
- [x] Quick Start
   - [x] Project Lib Architecture
   - [x] Model Established Pipeline
   - [x] Operation Mode
- [x] Other Records
- [x] Reference

Data science is a technology to extract the effective information from the large data. This repo enables you to have a quick understanding of the data science from web fund data scraping to EDA, and machine learning model established.

## Part II. Fundemental Principle
### Introduction to the Data Science 
In this project, we show the several functions of the tool as following figure:

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/DataScienceStudyFlow.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 1. Data Science Flow Chart
</div>
<!-- blank line -->

Data science is a multi-disciplinary field that uses scientific methods, processes, algorithms and systems to extract knowledge and insights from structured and unstructured data.
There are five steps to established the prediction system from the large data, as Figure 1:
- [1] Problem Definition:  Using the professional view to define the problem and the objective of the solving of the problem
- [2] Exploratory Data Analysis, EDA: First, we use the statistics methods to analyze the data sets to summerize the main characteristics from data collection and data clean. Second, we use the principle component analysis to analyze the features importance and the features selection.
- [3] Feature Engineering: Converting the raw data to the features and using these features to established the model.
- [4] Model Established: Spliting the data to the training, the testing and the validation data to established the model, select the model and optimize the model. 
- [5] Model Maintenance: Runtime model implementation, monitoring, and the diagnosis.

### Fund Data Analysis Overview [1]
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/FinanceManagerProjectOverview.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 2. Finance Manager Project Overview
</div>
<!-- blank line -->

In this project, we cover two parts, one is the data collection, the another is to establihed the fund short value estimation model, as Figure 2..
In fund analysis, large historical data stored in the cloud, it is difficult to clone these data; in this project, we use this tool to scraping from [web (鉅亨網)](https://fund.cnyes.com/) fund data and use these data to analyze the future value. On the other hand, we store these data into the local database [H5 format file] (https://www.hdfgroup.org/solutions/hdf5/) and the private cloud database, ie. [mongoDB](https://www.mongodb.com/atlas-signup-from-mlab?utm_source=google&utm_campaign=gs_apac_taiwan_search_brand_atlas_desktop&utm_term=mlab&utm_medium=cpc_paid_search&utm_ad=e&gclid=EAIaIQobChMIu9f4idKN5wIVWqWWCh0Flw1vEAAYASAAEgKa7vD_BwE).
There are many types in fund, such as bond, stock, currency and etc. More accuracy in analysis, more complex in different fund data analysis. In this project, we fucus on the fund short value estimation; first, we clone all historical data from cloud, then we convert these data to eleven finance index as the features. Last, we use the two types methods to establihed model, one is the time series model, such as the ARIMA model; the another is using the Facebook time series method to analyze the data from [Facebook public module- prophet](https://facebook.github.io/prophet/docs/quick_start.html). 

## Part III. Data Analysis through the Data Science 
### From EDA to Model Established
From web scraping the fund data, we got the date and the net asset value (NAV) of the fund. Converting the NAV to the finance index as following as table[1-2]: 

    | No. | Variables                       | Data Type         | Description                                                                                                                                                                                  |
    |-----|---------------------------------|-------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|	
    | 1   | Quartile                        | numerical, float  | A quartile is a statistical term describing a division of observations into four defined intervals based upon the values of the data and how they compare to the entire set of observations. |
	| 2   | Moving Average, MA              | numerical, float  | A series of averages of different subsets.                                                                                                                                                   |
	| 3   | Parabolic Stop and Reverse, SAR | numerical, float  | Used by traders to determine trend direction and potential reversals in price.                                                                                                               |
	| 4   | Psychological Line, PSY         | numerical, float  | The ratio of the number of rising periods over the total number of periods.                                                                                                                  |
	| 5   | On Balance Volume, OBV          | numerical, float  | A cumulative running total of the amount of volume occurring on up periods compared to down periods.                                                                                         |
	| 6   | Stochastic Oscillator, KD       | numerical, float  | Normalizing price as a percentage between 0 and 100.                                                                                                                                         |
	| 7   | Williams Percent Range, WR      | numerical, float  | Similar to a stochastic oscillator, as it normalizes the price as a percentage between 0 and 100.                                                                                            |
	| 8   | Momentum, MTM                   | numerical, float  | This indicator identifies when the price is moving upwards or downwards, and by how much.                                                                                                    |
	| 9   | Relative Strength Index, RSI    | numerical, float  | This indicator measures the magnitude of recent price changes to evaluate overbought or oversold conditions in the price of a stock or other asset.                                          |
    | 10  | Oscillator, OSC                 | numerical, float  | Using the trend indicator to discover short-term overbought or oversold conditions.                                                                                                          |
    | 11  | Bias                            | numerical, float  | An illogical preference or prejudice.                                                                                                                                                        |

<div align="center">

Table 1. Finance Index Information
</div>
<!-- blank line -->
<!-- blank line -->

From the Table 1, these 11 indicators represent the information between the historical information and current net value. On the other hand, we could use these indicators to judge the wheather the fund marketing go up or not. Therefore, we convert these indicators to the degree of the long/short marketing; there are 10 level represent the degree of the bull/bear market, it represent the bull marketing as the value is close to 10; vice versa.

Using these indicators, we would establihed elvaluation model [3-11]: 
1. Using Time Series Analysis to Established Model:
We use the date as the input and the NAV as the output to established the estimation model.
- Seasonal Components Decomposition [6-7]:  
  Seasonal decomposition using moving averages, and then time series are usually decomposed into:  
  $`T_{t}`$, the trend component at time t, which reflects the long-term progression of the series (secular variation). A trend exists when there is a persistent increasing or decreasing direction in the data. The trend component does not have to be linear.  
  $`C_{t}`$, the cyclical component at time t, which reflects repeated but non-periodic fluctuations. The duration of these fluctuations is usually of at least two years.  
  $`S_{t}`$, the seasonal component at time t, reflecting seasonality. A seasonal pattern exists when a time series is influenced by seasonal factors. Seasonality occurs over a fixed and known period (e.g., the quarter of the year, the month, or day of the week).  
  $`I_{t}`$, the irregular component (or "noise") at time t, which describes random, irregular influences. It represents the residuals or remainder of the time series after the other components have been removed.  
  There are two types of the seasonal components decomposition:  
  - Additive Model: If $`y_{i}`$ is the corresponding true value for the time series, then additive model is  
    $`y_{i} = T_{t}+C_{t}+S_{t}+I_{t}`$  
  - multiplicative model: If $`y_{i}`$ is the corresponding true value for the time series, then additive model is   
    $`y_{i} = T_{t}C_{t}S_{t}I_{t}`$ or $`logy_{i} = logT_{t}+logC_{t}+logS_{t}+logI_{t}`$  

- Seasonal ARIMAX Model [6, 8-11]:
  - Augmented Dickey-Fuller Test  
    The null hypothesis of the Augmented Dickey-Fuller is that there is a unit root, with the alternative that there is no unit root. If the pvalue is above a critical size (Based on MacKinnon, 2010), then we cannot reject that there is a unit root.  
  - ACF/PACF plot  
    ACF is a completed auto-correlation function which gives us values of auto-correlation of any series with its lagged values.  
    PACF is a partial auto-correlation function. Basically instead of finding correlations of present with lags like ACF, it finds correlation of the residuals (which remains after removing the effects which are already explained by the earlier lag(s)) with the next lag value hence ‘partial’ and not ‘complete’ as we remove already found variations before we find the next correlation.  
  - Model Established  
    A seasonal ARIMA model is formed by including additional seasonal terms in the ARIMA models we have seen so far. It is written as follows:  
    $`ARIMA(p,d,q)(P,D,Q)_{m}`$  
    where   
    $`m =`$ number of observations per year. We use lowercase notation for the non-seasonal parts of the model (First Term), and uppercase notation for the seasonal parts of the model (Second Term).  
    $`p =`$ order of the autoregressive part of the non-seasonal part. Order p is the lag value after which PACF plot crosses the upper confidence interval for the first time from the non-seasonal part.  
    $`d =`$ degree of first differencing involved of the non-seasonal part. Stationary after order-d difference for the non-seasonal part.  
    $`q =`$ order of the moving average part of the non-seasonal part. Order q the lag after which ACF crosses the upper confidence interval for the first time from the non-seasonal part.  
    $`P =`$ order of the autoregressive part of the seasonal part. Order P is the lag value after which PACF plot crosses the upper confidence interval for the first time from the seasonal part.  
    $`D =`$ degree of first differencing involved of the seasonal part. Stationary after order-D difference for the seasonal part.  
    $`Q =`$ order of the moving average part of the seasonal part. Order Q the lag after which ACF crosses the upper confidence interval for the first time from the seasonal part.  
    For the non-seasonal part in ARIMA, given a time series data $`y_t`$, the full model ARIMA(p, q, d) can be written as  
    $`(1-\sum_{i=1}^{p}\phi_{i}L^i)\delta_dy_t=c+(1-\sum_{i=1}^{q}\theta_{i}L^i)\epsilon_t`$  
    where $`L`$ is the lag operator, $`\delta_d`$ is the d order-difference operator (I model), the $`\phi`$ are the parameters of the autoregressive part of the model (AR model), the $`\theta`$ are the parameters of the moving average part (MA model) and the $`\epsilon _{t}`$ are error terms.
        Similaryly, we can find the seasonal part from the seasonal components in time series in the same method.  

2. Using FB Prophet Method to Established Model     
We use the date as the input, the NAV as the output and all indicators as the time series change point to established the estimation model.  
- Seasonal Components Decomposition   
  In FB-prophet module, use the additive model or the multiplicative model to analyze the model for the overall trend, weekly trend and yearly trend.  
- Model Establihsed   
  There are two types trend model, one is the linear model with the change points and another is the logistic growth model with change points. We incorporate trend changes in the growth model by explicitly defining changepoints where the growth rate is allowed to change.   
  For prophet model, the model rely on Fourier series to provide a exible model of periodic effects, including the weekly and yearly components. Moreover, adding some seasonality as the  holidays and events by generating a matrix of regressors.   
  To account for that we include additional parameters for the days surrounding the holiday, essentially treating each of the days in the window around the holiday as a holiday itself.  

Last, we use the some error fromulas to measure the model evaluation [12-13]:
- Mean Squart Error, MSE:
  - A risk metric corresponding to the expected value of the squared (quadratic) error or loss.
  - If $`\hat{y_{i}}`$ is the predicted value of the $`i`$-th sample, and y_{i} is the corresponding true value, then the mean squart error (MSE) estimated over $`n_{sample}`$ is defined as  
     $`MSE(y, \hat{y}) = \frac{1}{n_{sample}}\sum_{i=0}^{n_{sample-1}}(y_{i}-\hat{y})^2`$
  - Lower values are better.
- Root Mean Squart Error, RMSE
  - Root of the mean squart error.
  - Lower values are better.
- Mean Absolute Error, MAE
  - A risk metric corresponding to the expected value of the absolute error loss or $`l1`$-norm loss.
  - If $`\hat{y_{i}}`$ is the predicted value of the $`i`$-th sample, and y_{i} is the corresponding true value, then the mean absolute error (MAE) estimated over $`n_{sample}`$ is defined as  
     $`MAE(y, \hat{y}) = \frac{1}{n_{sample}}\sum_{i=0}^{n_{sample-1}}\left | y_{i}-\hat{y} \right |`$
  - Lower values are better.
- Explained Variance Score:
  - Explained variation measures the proportion to which a mathematical model accounts for the variation (dispersion) of a given data set.
  - If $`\hat{y}`$ is the estimated target output,  $`y`$ the corresponding (correct) target output, and $`Variance`$ is the square of the standard deviation, then the explained variance is estimated as follow:  
     $`Explained\ Variance\ Score(y, \hat{y}) = 1- Variance(y-\hat{y})/Variance(y)`$
  - The best score is 1.0. Lower values are worse.
- R2 Score
  - It represents the proportion of variance (of y) that has been explained by the independent variables in the model. It provides an indication of goodness of fit and therefore a measure of how well unseen samples are likely to be predicted by the model, through the proportion of explained variance.
  - If $`\hat{y}`$ is the predicted value of the $`i`$-th sample and $`y_{i}`$ is the corresponding true value for total $`n`$ samples, then the r2 is defined as follow:  
     $`r2(y, \hat{y}) = 1- \frac{\sum_{i=1}^{n}(y_{i}-\hat{y})^{2}}{\sum_{i=1}^{n}(y_{i}-mean(y))^{2}}`$ 
  - The best score is 1.0. Lower values are worse.

### Simple Model Example
In this project, we analyze the Templeton Emerging Markets Fund data from the web scraping, date time from 2009/8/14 to 2020/1/2. First, we look at the NAV plot:

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/TempletonEmergingMarkets_All_train_line.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 3. Templeton Emerging Markets Fund Historical Net Asset Value
</div>
<!-- blank line -->

We convert the NAV data to the finance indicator and the bull/bear levels, we investigate the numerical data as following as figure and table:

<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/TempletonEmergingMarkets_All_train_finance_indicator_line.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 4. Templeton Emerging Markets Fund Finance Indicators
</div>

	|       |  NAV	  | Q1	    | Q2	  | Q3	    | MA, Week	| MA, Half of Month	| MA, Month	| MA, Season  | MA, Year | SAR-10  | PSY-12	 | OBV	    | K	      | D	    | WR-5	   | WR-10	   | WR-15	   | MTM	  | RSI-5	| RSI-10	| OSC	    | Bias-5   | Bias-10 | Bias-20	| Bias-70 | 
	|-------|---------|---------|---------|---------|-----------|-------------------|-----------|-------------|----------|---------|---------|----------|---------|---------|----------|-----------|-----------|----------|---------|-----------|-----------|----------|---------|----------|---------|
    | count	| 2679.00 | 2679.00 | 2679.00 | 2679.00 | 2679.00	| 2679.00	        | 2679.00	| 2679.00	  | 2679.00	 | 2679.00 | 2679.00 | 2679.00	| 2679.00 | 2679.00	| 2677.00  | 2679.00   | 2679.00   | 2679.00  | 2679.00	| 2679.00	| 2679.00	| 2679.00  | 2679.00 | 2679.00	| 2679.00 | 
    | mean	| 10.00	  | 10.36	| 11.13	  | 11.49	| 9.99	    | 9.97	            | 9.94	    | 9.81	      | 9.21	 | 6.40	   | 45.71	 | 1206.00	| 51.11	  | 51.04	| 51.69	   | 51.06	   | 50.67	   | 0.03	  | 31.82	| 31.73	    | 8.80e-06  | -0.03	   | -0.06	 | -0.14	| -0.58   | 
    | std	| 1.55	  | 0.79	| 0.26	  | 0.27	| 1.60	    | 1.65	            | 1.75	    | 2.11	      | 3.19	 | 3.77	   | 17.36	 | 355.97	| 31.56	  | 28.10	| 43.35	   | 39.32	   | 37.83	   | 0.63	  | 35.71	| 35.69	    | 0.36	    | 0.54	   | 0.84	 | 1.30	    | 2.50    | 
    | min	| 6.64	  | 8.87	| 9.99	  | 9.99	| 0.00	    | 0.00	            | 0.00	    | 0.00	      | 0.00	 | 0.00	   | 0.00	 | 0.00	    | 0.00	  | 0.00	| 0.00	   | -1.42e-14 | -1.42e-14 | -0.76	  | 0.00	| 0.00	    | -7.68	    | -3.21	   | -4.58	 | -6.29	| -9.76   | 
    | 25%	| 8.87	  | 9.58	| 10.92	  | 11.40	| 8.87	    | 8.86	            | 8.84	    | 8.82	      | 8.76	 | 2.45	   | 33.33	 | 1102.97	| 22.05	  | 27.34	| 0.00	   | 9.09	   | 13.33	   | -0.09	  | 0.00	| 0.00	    | -0.01	    | -0.30	   | -0.45	 | -0.75	| -1.86   | 
    | 50%	| 10.61	  | 10.82	| 11.18	  | 11.54	| 10.60	    | 10.60	            | 10.57	    | 10.51	      | 9.79	 | 9.56	   | 50.00	 | 1324.18	| 51.46	  | 50.72	| 50.00	   | 50.00	   | 50.00	   | 0.00	  | 0.00	| 0.00	    | 0.00	    | 0.02	   | 0.01	 | -0.01	| -0.21   | 
    | 75%	| 11.35	  | 10.94	| 11.36	  | 11.64	| 11.34	    | 11.35	            | 11.33	    | 11.29	      | 11.38	 | 10.115  | 8.33	 | 1456.14	| 81.29	  | 76.40	| 100.00   | 94.74	   | 90.48	   | 0.09	  | 66.67	| 66.67	    | 0.01	    | 0.29	   | 0.45	 | 0.67	    | 1.12    | 
    | max	| 12.25	  | 11.08	| 11.48	  | 11.84	| 12.24	    | 12.22	            | 12.18	    | 12.14	      | 11.90	 | 10.151  | 00.00	 | 1652.59	| 99.97	  | 99.87	| 100.00   | 100.00	   | 100.00	   | 10.15	  | 96.67	| 96.67	    | 8.05	    | 2.98	   | 2.97	 | 4.17	    | 4.67    | 

<div align="center">

Table 2. Templeton Emerging Markets Fund Finance Indicators Investigation
</div>

From Figure 4. and Table 2., we interpret that the Indicator-Number is meaning for the number of the days of the moving window in the indicator calculation. 
From above figures and tables, we find that:
- There are no missing data in the historical data (Figure 3.).
- From 2009 to 2015. the fund NAV is hihger than 10; from 2015 to 2020, the fund NAV is lower than 10; turning point may be happen in 2015 (Figure 3. and Table 2.).
- Trend reflect on the quartile and moving average (Figure 4. and Table 2.).
- There are no large variation from finance indicators (Figure 4. and Table 2.).

We extract the 7:3 training/ validation data set from the training data. Use the 2 methology to establihed model, one is the time series analysis and the another is Facebook method.

1. Using Time Series Analysis to Established Model    
First, from Figure 3., we see the some bit seasonal component in the time series; therefore, we could use the additive model for the seasonal decompostion to investigate the time series as following as figure:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/TempletonEmergingMarkets_All_train_tsDecomposition.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 5. Seasonal Decomposition for the Templeton Emerging Markets Fund
</div>

From Figure 5., we summerize some tips:
- Trend first go up and then go down.
- Seasonal component are the periodic in around the one year.
- Magnitude of the residual are more than the seasonal component.

Second, we use the the Augmented Dickey-Fuller Test to anlyze the time series, we discover that:
- The number of observations used for the ADF regression and calculation of the critical values: 2677
- ADF score: 0.35
- P value: 0.98
- Critical values: 1%: -3.43, 5%: -2.86, 10%: -2.57

This time series is not stationary (ADF Score > Critical Values, P value > 0.05), we do the first order difference with this time series:
- The number of observations used for the ADF regression and calculation of the critical values: 2677
- ADF Score: -42.32
- P value: 0.0
- Critical values: 1%: -3.43, 5%: -2.86, 10%: -2.57 
From this result, we get the series is the stationary time series (ADF Score < Critical Values, P value < 0.05).

Next, we look into the ACF/PACF plot from the original time series and the time series with one order difference:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/TempletonEmergingMarkets_All_train_tsBasic.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 6. ACF and PACF plot for the Templeton Emerging Markets Fund
</div>

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/TempletonEmergingMarkets_All_train_tsDiffBasic.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 7. ACF and PACF plot for the Templeton Emerging Markets Fund with First-Order Difference
</div>

From Figure 6. and Figure 7., we summerize that:
- Same as the Dickey-Fuller test
- We know original time series is the non-stationary series with underdifference becacuse all lag-1 are positive in PACF plot, so we process the series with first order difference (Figure 7.).
- First 2-3 lags are significent in ACF plot, and first 2 lags are significent in PACF plot. Therefore, the order of the AR model is around the second order, and the order of the MA model is around third order (Figure 6. and Figure 7.). 
- Because some lags are negative, we need to grid search the best parameters of the ARIMA model (Figure 6.-Figure 7.). 

We use the date and NAV in the training data as the model input and the output. For grid search, set the parameters range from the ACF/PACF plot to find the model and adjust the parameters. Last, we use the validation data to measure the model.
Becasue there are many data point in the training data, we use the mean of the month to find the seasonal components.
According to the Figure 6. and Figure 7., we find the model SARIMA(3, 0, 0)(1,3,1)1 (NAV_SARIMAX_51 model) as the best model after grid search as following as figure:

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/NAV_SARIMAX_51_ForecastPlot.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 8. Templeton Emerging Markets Fund Validation Evaluation Net Asset Value Plot from SARIMA Time Series Model Established
</div>

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/NAV_SARIMAX_51_ValidationEvaluation.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 9. Templeton Emerging Markets Fund Evaluation Plot for the NAV_SARIMAX_51 Model
</div>

From Figure 8. and Figure 9. we summarize that
- The error evaluation is high around 400 days (Figure 8.). 
- Good model fit is after 400 days from MSE (0.07), RMSE (0.26), MAE (0.20), Explained Variance Score (0.90 Score) and R2 Score (0.90 Score) (Figure 9.). 
- Short/ Long term are good prediction.

2. Using FB Prophet Method to Established Model     
In this segment, we use the FB module - Prophet to establihed model. 
First, we set the some change points from the finance indicators and set the seasonality as the yearly period. 
We use the date and NAV in the training data as the model input and the output. For grid search, set the parameters range from the ACF/PACF plot to find the model and adjust the parameters. Last, we use the validation data to measure the model.
We find the linear prophet model (prophet_NAV_0) as best model as following as figures:

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/NAV_0_ForecastPlot.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 10. Templeton Emerging Markets Fund Validation Evaluation Net Asset Value Plot from Prophet Time Series Model Established
</div>

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/NAV_0_val_err_plot.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 11. Templeton Emerging Markets Fund Evaluation Plot for the prophet_NAV_0 Model
</div>
 
From above figures, we summarize some tips:
 - Validation error trend slowly become low, good long term prediction (Figure 10.).
 - About 400 days, the prophet_NAV_0 Model are better prediction from the MSE (0.09), RMSE (0.27), MAE (0.24), Explained Variance Score (0.87 Score) and R2 Score (0.86 Score) (Figure 11.).

And we could investigate the overall/yearly/weekly trend variation based on the time series decomposition theory as following figures:

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/NAV_0_ForecastComponentsPlot.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 12. Templeton Emerging Markets Fund Components Plot for the prophet_NAV_0 Model
</div>

From Figure 7., we know that:
- From linear model prophet_NAV_0, high value is on Friday, and secondly on Monday, the rest on Saturaday and Sunday. High value for yearly variation is in Mid-Year. Trend are from high to low.
- Similar to the last part analysis.

## Part IV. Quick Start   
### Project Lib Architecture  
In this project, we need four librarys:
- FinanceManager: 
  - finance_main: main file (python file)
  - finance_preprocessing: some functions are about EDA and data preprocessing (python file)
  - finance_model_creator: some functions are about training/ testing/ validation (python file)
  
- Finance Lib:
  - getFundMainFunc: Web scraping fund data from Cnyes web, web scraping exchange rate data from Taiwan bank
  - cnyesFund: Web scraping fund data interface lib (python file)
  - ExchangeRateManager: Web scraping exchange rate data interface lib (python file)
  - getFundStatistics: Finance indicators caculation lib (python file)

- CommonLib: Common Utility Tool (Note: Using this library, it is important to setup for some cython files!)
  - H5FileManager: .h5 interface lib file (cython file)
  - DataCircularMatrix: data ring temp buffer lib file (cython file)
  - MongoDBManager: Online MongoDB interface lib file (python file)

- MachineLearningLib: Various Machine Learning Model
  - Exploratory Data Analysis:  some functions about Exploratory Data Analysis (python file)
  - Learning Analysis: some features analysis by using machine learning (python file)
  - Model Creator Helper: Some training/ testing/ validation functions, including fbprophet and time series method (python file)
  - Model Evaluation: Some model evaluation functions (python file)
  - Preprocessing Utility: Data Preprocessing (python file)
  - Sampling Lib: Some sampling technique (python file)
  - MultiModelTool: Some models organization (python file)

### Model Established Pipeline
In this project, there are eights steps for fund datasets value estimation model established.
<!-- blank line --> 

#### Model System Prediction for Titanic Problem
- set_finance_database: Web scraping finance data and save the data to the database.
- set_finance_database_variables: Calculate the finance indicators from the fund data.
- exploratory_data_analysis_stage: Excute the EDA for variables in train and test file.
- features_selection_engineering: Features selections through the feature engineering and analysis.
- learning_analysis: Using the learning analysis to anlyze the features.
- ts_training_stage: Using the time series method to established the estimation model.
- fbprophet_training_stage: Using the FB prophet method to established the estimation model.
- model_organization: Factors of the models analysis.

### Operation Mode
#### IDE mode:
Excute the titanic_main.py in different IDE.
#### CMD mode:
- Set cmd_config.txt file for model prediction system established.
- Excute command line in windows cmd, such as the enable_hw.bat file.

## Part V. Other Records
### Some Notes
If you have any questions or suggestions, please let me know, thank you. Enjoy it!
<!-- blank line --> 

### History Notes
     DataVer    Author      Note
     20181003 - C.Y. Peng - First Release (20181003-R 1.0.0)
     20190212 - C.Y. Peng - Second Release, MongoDB Data Strore (20190212-R 1.0.0)
     20190221 - C.Y. Peng - Setting: Output Folder and ML Prediction
     20190309 - C.Y. Peng - addFinance Data Scraping and Analysis Tool Version - Release
     20200107 - C.Y. Peng - cmd mode
     20200114 - C.Y. Peng - ML model training
     20200209 - C.Y. Peng - Add the time sereis analysis
     20200216 - C.Y. Peng - Third Release (20200216-R 1.0.0)
	 
## Part VI. Reference
- [1] [Investopedia: Financial Term Dictionary](https://www.investopedia.com/financial-term-dictionary-4769738) , Investopedia Website. 
- [2] [Trading Technical](https://library.tradingtechnologies.com/trade/chrt-technical-indicators.html) , Trading Website.
- [3] Rob J Hyndman and George Athanasopoulos, [Forecasting: Principles and Practice](https://otexts.com/fpp2/) , online version book, February 2020.
- [4] Sean J. Taylor and Benjamin Letham, "Forecasting at Scale, " Facebook.
- [5] [Facebook public module- prophet](https://facebook.github.io/prophet/docs/quick_start.html) , Facebook Github.
- [6] [Python Public Module - Statsmodels](https://www.statsmodels.org/stable/index.html) , Statsmodels Documents.
- [7] Free Encyclopedia, [Decomposition of time series](https://en.wikipedia.org/wiki/Decomposition_of_time_series) , Wikipedia.
- [8] Free Encyclopedia, [Augmented Dickey-Fuller Test](https://en.wikipedia.org/wiki/Augmented_Dickey%E2%80%93Fuller_test) , Wikipedia.
- [9] Jayesh Salvi, (Significance of ACF and PACF Plots In Time Series Analysis)(https://towardsdatascience.com/significance-of-acf-and-pacf-plots-in-time-series-analysis-2fa11a5d10a8).
- [10] Gurchetan Singh, [7 methods to perform Time Series forecasting](https://www.analyticsvidhya.com/blog/2018/02/time-series-forecasting-methods/).
- [11] Eureka, [利用python进行时间序列分析——季节性ARIMA](https://zhuanlan.zhihu.com/p/35282988).
- [12] [Model Evaluation](https://scikit-learn.org/stable/modules/model_evaluation.html#regression-metrics) , Sklearn Documents.
- [13] Free Encyclopedia, [Explained Variance Score](https://en.wikipedia.org/wiki/Explained_variation) , Wikipedia.
