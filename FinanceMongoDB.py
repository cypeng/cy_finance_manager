"""
Created on 20190210
@author: user009
DataVer  - Author - Note
20190210 - C.Y. Peng - First Release
"""
from sys import path
path.append('../../CommonLib/')
import MongoDBManager

def check_mongoDB_database(file_name, client, database_name):
    try:
        collection_name = file_name + "_H52MongoDB_Grid"
        database = client[database_name]
        #collection = database[collection_name]

        if (collection_name + ".chunks" in database.collection_names()) and (collection_name + ".files" in database.collection_names()):
            print("Database Exists on MongoDB!")
            load_data_from_mongoDB(file_name, client, database_name)
    except:
        print("No Database Exists on MongoDB!")
        pass

def upload_data_to_mongoDB(file_name, client, database_name):
    print('Waiting for Uploading the Data to MongoDB!')
    collection_name = file_name + "_H52MongoDB_Grid"
    data_dict = {
        "file": file_name +".h5",
    }
    MongoDBManager.gridfs_to_mongo(client, database_name, collection_name, file_name+".h5", data_dict)

def load_data_from_mongoDB(file_name, client, database_name):
    collection_name = file_name + "_H52MongoDB_Grid"
    MongoDBManager.mongo_to_gridfs(client, database_name, collection_name, file_name+".h5")
