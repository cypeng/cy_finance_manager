'''
@Author: CY Peng
@Date: 2020-01-03
@LastEditTime: 2020-01-03

DataVer  - Author - Note
2020/01/12 C.Y.     Initialization
'''

import pandas as pd
import os
import numpy as np

import SamplingLib as SM
import fbprophetModelCreatorHelper as fMCH
import regModelCreatorHelper as MCH
import tsModelCreatorHelper as tsMCH
import ModelSelector as MS
import finance_preprocessing as FP

import pickle
from configparser import ConfigParser

from mlxtend.feature_selection import ColumnSelector
from mlxtend.classifier import EnsembleVoteClassifier
from sklearn.pipeline import make_pipeline

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    return data

def find_change_points_list(df_data, train_size = 0.77):
    #print(df_data.loc[df_data['Change_Point']==1,'Date'])
    length = df_data.shape[0]
    train_idx = int(length*train_size)
    train_data = df_data.loc[0:train_idx, ['Date', 'Change_Point']]
    #print(train_data)
    return train_data.loc[train_data['Change_Point'] == 1,'Date']

def ts_cv_training_stage(filename, database_folder, train_file_tail_tag = None,
                         features_engineering_model_path = os.path.join(".","EDA_output","train"),
                         model_config_filename='models_config_test'):
    # Model Selection
    grid_param =  MS.ts_model_grid_param_search(os.path.join(os.path.dirname(os.path.realpath(__file__)), model_config_filename))
    #print(grid_param)
    #print(grid_param[0][0],grid_param[1])

    # Data Read
    rawData = read_data(os.path.join(database_folder, filename))
    raw_y = rawData.loc[:, 'y'].to_frame()
    raw_x = rawData.drop(columns=['y'], axis=1)

    # Training Stage
    train_size = 0.77
    #print(grid_param)
    ModelHelper = tsMCH.tsMLHelper(grid_param[0][0],grid_param[1])
    ModelHelper.data_preparation(raw_x, 
	                             raw_y, 
                                 train_file_tail_tag, 
                                 features_engineering_model_path, 
                                 FP.FeaturesEngineeringFit,
                                 train_drop_column = None,
								 train_size = train_size)
    ModelHelper.cv_train(ds_column = ['Date'], ts_model_column = ['NAV'], resample = None)

def fbprophet_cv_training_stage(filename, database_folder, train_file_tail_tag = None,
                                features_engineering_model_path = os.path.join(".","EDA_output","train"),
                                model_config_filename='models_config_test'):
    # Parameters Setting
    cv_params = {}
    cv_params['initial'] = '1825 days'
    cv_params['period'] = '365 days'

    # Model Selection
    grid_param =  MS.prophet_grid_param_search(os.path.join(os.path.dirname(os.path.realpath(__file__)), model_config_filename))
    #print(grid_param)

    # Data Read
    rawData = read_data(os.path.join(database_folder, filename))
    raw_y = rawData.loc[:, 'y'].to_frame()
    raw_x = rawData.drop(columns=['y'], axis=1)
    
    # Find Change Point
    train_size = 0.77
    rawDataFeatureEngineering = read_data(os.path.join(database_folder, filename+'_features_engineering'))
    changepoints=find_change_points_list(rawDataFeatureEngineering, train_size = train_size)
    grid_param['changepoints'] = [changepoints]
    print(grid_param)

    # Training Stage
    ModelHelper = fMCH.fbprophetMLHelper(grid_param)
    ModelHelper.data_preparation(raw_x, 
	                             raw_y, 
                                 train_file_tail_tag, 
                                 features_engineering_model_path, 
                                 FP.FeaturesEngineeringFit,
                                 train_drop_column = None,
								 train_size = train_size)
    ModelHelper.cv_train(ds_column = ['Date'], ts_model_column = ['NAV'], **cv_params)

def test():
    ds_column = ['Date']
    y_column = ['NAV']
    x_train = read_data(r'.\CompletedModel\SimpleModel0\Prophet_0\x_train')
    x_val = read_data(r'.\CompletedModel\SimpleModel0\Prophet_0\x_val')
    df_train_type = {'ds': sum(x_train[ds_column].values.tolist(),[]),
                     'y': sum(x_train[y_column].values.tolist(),[])}
					 #'cap': max(sum(x_train[y_column].values.tolist(),[])),
					 #'floor': min(x_train[y_column].values.tolist(),[])}
                  #print(df_train_type)
    df_train = pd.DataFrame(data = df_train_type) #, index = False
    
    df_val_type = {'ds': sum(x_val[ds_column].values.tolist(),[]),
                   'y': sum(x_val[y_column].values.tolist(),[])}
    		       #'cap': max(x_val[y_column].values.tolist(),[]),
    		       #'floor': min(x_val[y_column].values.tolist(),[])}
    df_val = pd.DataFrame(data = df_val_type)

    print(df_val, df_train)
    #df_cv_table, df_cv_performance, _ = prophet_cross_validation(output_path, filename, None)
    df_val_forecast, df_val_forecast_performance, _ = fMCH.cal_pred_error(df_val, r'.\CompletedModel\SimpleModel0\Prophet_0', 'NAV')
    fMCH.predict_plot(df_val_forecast, df_train, df_val, r'.\CompletedModel\SimpleModel0\Prophet_0', 'NAV', ylabel_tag = 'NAV')

def nested_training_stage(filename, 
                          database_folder, 
						  train_file_tail_tag = None,
                          features_engineering_model_path = os.path.join(".","EDA_output","train"), 
                          model_config_filename='models_config_test'):
    # Model Selection
    regs, grid_param =  MS.reg_model_selector(os.path.join(os.path.dirname(os.path.realpath(__file__)), model_config_filename))

    # Data Read
    rawData = read_data(os.path.join(database_folder, filename))
    raw_y = (rawData.loc[:, 'y']-rawData.loc[:, 'NAV']).to_frame()#(rawData.loc[:, 'y']-rawData.loc[:, 'NAV']).to_frame() rawData.loc[:, 'y'].to_frame()
    raw_x = rawData.drop(columns=['y'], axis=1)

    # Training Stage
    ModelHelper = MCH.MultiModelMLHelper(regs, grid_param)
    outer_fold_obj, inner_fold_obj = SM.two_stages_timeseries_sampling_split_obj()
    ModelHelper.data_preparation(raw_x, 
                                 raw_y, 
							     train_file_tail_tag, 
							     features_engineering_model_path, 
							     FP.FeaturesEngineeringFit,
							     train_drop_column = ['Date', 'Bull/Bear_0Map', 'Bull/Bear_1Map','Bull/Bear_2Map'])
    ModelHelper.nested_train(inner_fold_obj, outer_fold_obj)
