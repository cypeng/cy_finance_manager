@echo [off]
::64 bit python
%CD%
set PYTHONPATH=%PYTHONPATH%;..\CommonLib;..\MachineLearningLib;.\financelib;
call E:\Anaconda3_64bit\envs\python_venv37g\Scripts\pip.exe freeze > requirements.txt
call E:\Anaconda3_64bit\envs\python_venv37g\python.exe finance_main.py cmd_config_test.txt
pause
@echo [on]